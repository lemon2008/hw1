from collections import defaultdict
from random import uniform
from math import sqrt
import sys
from numpy.random import choice
import matplotlib

matplotlib.use('Agg')

import matplotlib.pyplot as plt

def avg(points):
    
    dimensions = len(points[0])

    new_center = []

    for dimension in xrange(dimensions):
        dim_sum = 0  
        for p in points:
            dim_sum += p[dimension]

        # average of each dimension
        new_center.append(dim_sum / float(len(points)))

    return new_center


def update_centers(points, assignments):

    new_means = defaultdict(list)
    centers = []
    for assignment, point in zip(assignments, points):
        new_means[assignment].append(point)
        
    for points in new_means.itervalues():
        centers.append(avg(points))

    return centers


def assign_points(data_points, centers):

    assignments = []
    for point in data_points:
        shortest = ()  # positive infinity
        shortest_index = 0
        for i in xrange(len(centers)):
            val = distance(point, centers[i])
            if val < shortest:
                shortest = val
                shortest_index = i
        assignments.append(shortest_index)
    return assignments


def distance(a, b):
    dimensions = len(a)
    
    squre_distance = 0
    for dimension in xrange(dimensions):
        # dis(a,b) = (x1 - x2) ^ 2 + (y1 - y2) ^ 2
        
        diff_dim = (a[dimension] - b[dimension]) ** 2
        squre_distance += diff_dim
    return sqrt(squre_distance)


def initiate_centers(points, k):
    centers = []
    dimensions = len(points[0])
    min_max = defaultdict(int)
    
    for point in points:
        for i in xrange(dimensions):
            val = point[i]
            min_key = 'min_%d' % i
            max_key = 'max_%d' % i
            if min_key not in min_max or val < min_max[min_key]:
                min_max[min_key] = val
            if max_key not in min_max or val > min_max[max_key]:
                min_max[max_key] = val

    for dim in xrange(k):
        rand_point = []
        for i in xrange(dimensions):
            min_val = min_max['min_%d' % i]
            max_val = min_max['max_%d' % i]
            
            rand_point.append(uniform(min_val, max_val))

        centers.append(rand_point)

    return centers
def DX(centers, x):
    val = 100000000
    for m in centers:
        if distance(m,x) < val:
            val = distance(m,x) ** 2
    return val
def probility(centers, xs):
    dxs = 0
    # get the sum of the dxj
    for x in xs:
        dxs += DX(centers, x)
    probility_dist = []
    for x in xs:
        pro = DX(centers, x) / float(dxs)
        probility_dist.append(pro)
    return probility_dist
    
def initiate_centers_pp(points, k):
    centers = []
    dimensions = len(points[0])
    # get the first center
    index = int(uniform(0, len(points)))
    centers.append(points[index])
    for i in range(1, k):
        probility_dist = probility(centers, points)
        draw = choice(range(0, len(points)), 1, p=probility_dist)
        centers.append(points[draw[0]])
    #print draw
    return centers
    
def distortion_value(centers, assignments, points):
    dist_value = 0
    for point in points:
        index = points.index(point)
        dist_value += distance(centers[assignments[index]], point) ** 2
    return dist_value
    
def k_means(points, k, plus = False):
    k_points = []
    if(plus):
        k_points = initiate_centers_pp(points, k)
    else:
        k_points = initiate_centers(points, k)
    assignments = assign_points(points, k_points)
    old_assignments = None
    count = 0
    new_centers = []
    while assignments != old_assignments :
        count = count + 1
        new_centers = update_centers(points, assignments)
        old_assignments = assignments
        assignments = assign_points(points, new_centers)
        new_dis = distortion_value(new_centers, assignments, points)
        #print old_dis, new_dis
        print new_dis
    return zip(assignments, points)




def read_points(file):
    points = []
    with open(file) as f:
		for line in f:
			p = line.split(' ')
			poi = []
			# process the data read from specified file
			for v in p:
			    if(v != '' and v != '\n'):
			        poi.append(float(v))
			# print poi
			points.append(poi)
    return points
    
def main():
    points = read_points("toydata.txt")
    file="kmeans"
    if(len(sys.argv) == 2 and sys.argv[1] == 'plus'):
        clusters = k_means(points, 3, True)
        file="kmeanspp"
    else:
        clusters = k_means(points, 3)
    #print clusters
    for cluster in clusters:
        p = cluster[1]
        #print p
        if(cluster[0] == 0):
            plt.plot(p[0], p[1],'bo') 
            
        if(cluster[0] == 1):
            plt.plot(p[0], p[1],'ro') 
            
        if(cluster[0] == 2):
            plt.plot(p[0], p[1],'go') 
    plt.savefig(file)
    
if __name__=="__main__":
	main()
#points = [ [1, 2],[2, 1],[3, 1],[5, 4],[5, 5],[6, 5], [10, 8], [7, 9],[11, 5],[14, 9],[14, 14] ]
