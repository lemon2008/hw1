kmeans

===

kmeans ++

- kmeans.py
    - Usage: python kmeans.py [plus]
        - with plus, we are running kmeans plus
        - the input file is toy data, remaining to be implemented as paramter
- run.sh 
    - 20 separate run of kmeans to log/
    - draw the distortion to the file you want 
- runpp.sh
    - 20 separate run of kmeans++ to logpp/
    - draw the distortion to the file you want 
-draw.py
    - Usage: python draw.py dir pic 
    - take the distortion under the dir file as input to draw a picture named pic.png
    