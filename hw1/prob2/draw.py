import os
import matplotlib
import sys
matplotlib.use('Agg')

import matplotlib.pyplot as plt

def read_points(file):
    points = []
    with open(file) as f:
		for line in f:
			points.append(float(line))
    return points
def draw_dir(directory, name):
	files = os.listdir(directory)
	for f in files:
		points = read_points(directory + f)
		plt.plot(points)
	plt.savefig(name)
def main():
	if(len(sys.argv) == 3):
		draw_dir(sys.argv[1], sys.argv[2])
	else:
		draw_dir("log/", "k-means")

if __name__=="__main__":
	main()
