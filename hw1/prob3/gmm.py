
import random
import numpy as np
import matplotlib
from math import sqrt
matplotlib.use('Agg')


import matplotlib.pyplot as plt
import math
from scipy.stats import multivariate_normal


def distance(a, b):
    dimensions = len(a)
    
    squre_distance = 0
    for dimension in xrange(dimensions):
        # dis(a,b) = (x1 - x2) ^ 2 + (y1 - y2) ^ 2
        
        diff_dim = (a[dimension] - b[dimension]) ** 2
        squre_distance += diff_dim
    return sqrt(squre_distance)

def gmm(points,theta,mean,cov,num_iter):
    # E step
    gmm=[[] for _ in range(len(points))] #nx3 matrix
    distortion_arr=[]
    dist_arr = []
    prev=0.0
    for i in range(0,num_iter):
        print(i)
        for index in range(0,len(points)):
            norm = [0.0]*3
            for m_index in range(0,3):
                norm[m_index] = multivariate_normal.pdf(points[index], mean=mean[m_index], cov=cov[m_index])
            theta_norm = np.multiply(theta,norm)
            px=np.sum(theta_norm)
            gmm[index] = np.divide(theta_norm,px)

        # get the transpose of gmm array
        gmm_trans=np.transpose(gmm)
        # update theta
        for m_index in range(0,3):
            theta[m_index]=np.divide(np.sum(gmm_trans[m_index]),len(points))
            
        # compensate the not enough probility value
        if(np.sum(theta)<1):
            comp = 1-np.sum(theta)
            theta[0] += comp

        # Update mean
        for m_index in range(0,3):
            x_mean=np.divide(np.sum(np.multiply(gmm_trans[m_index],np.transpose(points)[0])),np.sum(gmm_trans[m_index]))
            y_mean=np.divide(np.sum(np.multiply(gmm_trans[m_index],np.transpose(points)[1])),np.sum(gmm_trans[m_index]))
            mean[m_index]=[x_mean,y_mean]

        # Update covariance
        for m_index in range(0,3):
            cov[m_index]=[[0.0,0.0],[0.0,0.0]]
            for n in range(0,len(points)):
                cov[m_index]=np.add(cov[m_index],np.multiply(np.dot(np.subtract(points[n],mean[m_index])[np.newaxis].T,np.subtract(points[n],mean[m_index])[np.newaxis]),gmm_trans[m_index][n]))
 
            cov[m_index]=np.divide(cov[m_index],np.sum(gmm_trans[m_index]))

        # M step
        distortion = [0.0]*3
        for m_index in range(0,3):
            for index in range(0,len(points)):
                norm = multivariate_normal.pdf(points[index], mean=mean[m_index], cov=cov[m_index])
                distortion[m_index] += gmm[index][m_index]*(math.log(theta[m_index])+math.log(norm))
        distortion = np.sum(distortion)
                
        if(len(distortion_arr) > 0 and np.sum(distortion) <= distortion_arr[len(distortion_arr) - 1]):
            break
        distortion_arr.append(distortion)

        # distance to center
        dist = [0.0] * 3
        for m_index in range(0,3):
            for index in range(0,len(points)):
                dist[m_index] += distance(points[index], mean[m_index]) ** 2
        dist_arr.append(np.sum(dist))
    return theta,mean,cov,distortion_arr, dist_arr
def read_points(file):
    points = []
    with open(file) as f:
		for line in f:
			p = line.split(' ')
			poi = []
			# process the data read from specified file
			for v in p:
			    if(v != '' and v != '\n'):
			        poi.append(float(v))
			# print poi
			points.append(poi)
    return points
def drawclusers(points,K,theta,mean,cov):
    colors=["blue","red","green"]
    fig = plt.figure()
    canvas = fig.add_subplot(111)
    for index in range(0,len(points)):
        norm=[0.0]*3
        for m_index in range(0,3):
            norm[m_index]=multivariate_normal.pdf(points[index], mean=mean[m_index], cov=cov[m_index])
        theta_norm=np.multiply(theta,norm)
        cluster_index=np.argmax(theta_norm)
        canvas.scatter(points[index][0],points[index][1],color=colors[cluster_index])
    fig.savefig("cluster")
        
def main():
  
    for pic_index in range(20):
        run = 100
        points = read_points("toydata.txt")
        points_trans=np.transpose(points)
        mean=[[] for _ in range(3)]
        # get the min and max value of x, y axis
        x_min=np.min(points_trans[0])
        x_max=np.max(points_trans[0])
        y_min=np.min(points_trans[1])
        y_max=np.max(points_trans[1])
        
        clusters_num = 3;
        
        for m_index in range(0,clusters_num):
            mean[m_index]=[random.uniform(x_min, x_max),random.uniform(y_min, y_max)]
        cov=[[[np.var(points_trans[0]),0.0],[0.0,np.var(points_trans[1])]]]*3
        theta=[0.333, 0.333, 0.333]
        if(np.sum(theta)<1):
            diff=1-np.sum(theta)
            theta[0]+=diff
        #theta,mean,cov,distortion_arr,dist_arr=gmm(points,theta,mean,cov,run)
        #fig1 = plt.figure()
        #pic = fig1.add_subplot(111)
        a,b,c,distortion_arr,dist_arr=gmm(points,theta,mean,cov,run)
        plt.plot(distortion_arr)
    #drawclusers(points,3,a,b,c)
    plt.savefig("distortion")
    return 

if __name__=="__main__":
	main()